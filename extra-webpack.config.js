const singleSpaAngularWebpack = require('single-spa-angular/lib/webpack').default;
const { merge } = require('webpack-merge');

// const StandaloneSingleSpaPlugin = require('standalone-single-spa-webpack-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (config, options) => {
  const singleSpaWebpackConfig = singleSpaAngularWebpack(config, options);

  // Feel free to modify this webpack config however you'd like to
  singleSpaWebpackConfig.devtool = 'source-map';

  return singleSpaWebpackConfig;
  // return merge(singleSpaWebpackConfig, {
  //   devServer: {
  //     // Not required, but it's often useful to run webpack-dev-server in SPA mode
  //     historyApiFallback: true
  //   },
  //   plugins: [
  //     new StandaloneSingleSpaPlugin({
  //       // required
  //       appOrParcelName: "angular",
  //     })
  //   ]
  // });
};
